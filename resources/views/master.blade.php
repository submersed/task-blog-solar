<!doctype html>
<html lang="en" ng-app="ApplicationBlog" ng-controller="ApplicationBlogController">
<head>
    <base href="/">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog</title>

    <!-- Angular's libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular-route.min.js"></script>

    <!-- Edit -->
    <link rel="stylesheet" href="css/xeditable.min.css">
    <script src="js/xeditable.min.js"></script>

    <!--Jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <!--Awesome Font -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

    <link rel="stylesheet" media="screen" href="css/theme.min.css">
    <link rel="stylesheet" media="screen" href="css/style.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <!-- Socket autobahn-->
    <script src="http://autobahn.s3.amazonaws.com/js/autobahn.min.js"></script>

    <!-- Application's scripts -->
    <script src="application/ng-app.js"></script>
    <script src="application/ng-login.js"></script>
    <script src="application/ng-dashboard.js"></script>
    <script src="application/ng-services.js"></script>
    <script src="application/ng-directives.js"></script>

</head>
<body>

<ng-view></ng-view>

</body>
</html>