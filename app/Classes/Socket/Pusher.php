<?php
/**
 * Created by PhpStorm.
 * User: sub
 * Date: 20.01.2017
 * Time: 1:56
 */

namespace App\Classes\Socket;

use App\Classes\Socket\Base\BasePusher;
use ZMQContext;

class Pusher extends BasePusher
{

    static function sendDataToServer(array $data){

        $context = new ZMQContext();
        $socket =$context->getSocket(\ZMQ::SOCKET_PUSH,'pusher');
        $socket->connect('tcp://127.0.0.1:5555');
        $data = json_encode($data);
        $socket->send($data);
    }


    public function broadcast($JsonDataForSend){

        $aDataForSend  = json_decode($JsonDataForSend,true);
        $subscribers = $this->getSubscribersTopic();

        if(isset($subscribers[$aDataForSend['topic_id']])){
            $topic = $subscribers[$aDataForSend['topic_id']];
            $topic->broadcast($aDataForSend);
        }


    }

}
