<?php

namespace App\Classes\Socket\Base;


use Illuminate\Support\Facades\Auth;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Ratchet\Wamp\WampServerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class BasePusher implements WampServerInterface
{

    protected $subscribersTopic = [];
    protected $usersSessions = [];


    public function addSubscribeTopic($topic)
    {
        $this->subscribersTopic[$topic->getId()] = $topic;
    }


    public function addUsersSession()
    {
        array_push($this->usersSessions, Auth::user());
    }


    /**
     * @return mixed
     */
    public function getSubscribersTopic()
    {
        return $this->subscribersTopic;
    }


    public function getUsersSessions(){
        return $this->usersSessions;
    }

    /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    function onOpen(ConnectionInterface $conn)
    {
        // TODO: Implement onOpen() method.
        echo json_encode($conn);
        echo "Open connection ($conn->resourceId)\n";
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    function onClose(ConnectionInterface $conn)
    {
        // TODO: Implement onClose() method.

        echo "Connection ($conn->resourceId) has been closed \n";
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    function onError(ConnectionInterface $conn, \Exception $e)
    {
        // TODO: Implement onError() method.
        echo "Pusher Error ($e->getMessage())";
        $conn->close();

    }

    /**
     * An RPC call has been received
     * @param \Ratchet\ConnectionInterface $conn
     * @param string $id The unique ID of the RPC, required to respond to
     * @param string|Topic $topic The topic to execute the call against
     * @param array $params Call parameters received from the client
     */
    function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        // TODO: Implement onCall() method.


        $conn->close();
        echo 'onCall';
    }

    /**
     * A request to subscribe to a topic has been made
     * @param \Ratchet\ConnectionInterface $conn
     * @param string|Topic $topic The topic to subscribe to
     */
    function onSubscribe(ConnectionInterface $conn, $topic)
    {

        // TODO: Implement onSubscribe() method.
        if (!array_key_exists($topic->getId(), $this->subscribersTopic)) {
            $this->subscribersTopic[$topic->getId()] = $topic;
        }
        echo " Subscribe on ($conn->resourceId) topic ($topic))";
    }

    /**
     * A request to unsubscribe from a topic has been made
     * @param \Ratchet\ConnectionInterface $conn
     * @param string|Topic $topic The topic to unsubscribe from
     */
    function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
        // TODO: Implement onUnSubscribe() method.

    }

    /**
     * A client is attempting to publish content to a subscribed connections on a URI
     * @param \Ratchet\ConnectionInterface $conn
     * @param string|Topic $topic The topic the user has attempted to publish to
     * @param string $event Payload of the publish
     * @param array $exclude A list of session IDs the message should be excluded from (blacklist)
     * @param array $eligible A list of session Ids the message should be send to (whitelist)
     */
    function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        // TODO: Implement onPublish() method.

        $conn->close();

        echo 'onPublish';
    }
}