<?php

/**
 * Created by PhpStorm.
 * User: sub
 * Date: 17.01.2017
 * Time: 21:42
 */
namespace App\Classes\Socket;

use App\Classes\Socket\Base\BaseSocket;
use Ratchet\ConnectionInterface;

class ChatSocket extends BaseSocket
{
    protected $clients;

    /**
     * DashboardSocket constructor.
     */
    public function __construct()
    {
        $this->clients = new \SplObjectStorage();
    }

    function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        echo  "New Connection {$conn->resourceId}  \n";
    }

    function onClose(ConnectionInterface $conn)
    {
        echo  "Connection {$conn->resourceId} has closed \n";
        $this->clients->detach($conn);
    }

    function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo  "Dashboard socket connection error {$e->getMessage()}\n";
        $conn->close();
    }

    function onMessage(ConnectionInterface $from, $msg)
    {

        foreach ($this->clients as $client) {
            $client->send($msg);
        }

    }


}