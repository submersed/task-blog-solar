<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($comment) {
            $comment->children()->delete();
        });
    }

    /*public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('l jS \\of F Y h:i:s A');
    }*/

    public function children()
    {
        return $this->hasMany(Comment::class, 'parent_id')->with('children')->with('user')->with('post')->with('like');
    }

    public function parent(){
        return $this->belongsTo(Comment::class, 'parent_id')->where('parent_id',null)->with('parent');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function like() {
        return $this->hasMany(Like::class,'target_id')->where('type','CMNT')->with('user');
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public static function  getCommentById($id){
        return self::where('id',$id)->first();
    }
}
