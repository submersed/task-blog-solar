<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Like extends Model
{


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function comment()
    {
        return $this->belongsTo(Comment::class,'target_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'target_id');
    }

    public static function getLike($post_id)
    {
        return self::where('target_id', $post_id)->where('type', 'POST')->where('user_id', Auth::user()->id)->first();
    }

    public static function getCommentLike($comment_id)
    {
        return self::where('target_id', $comment_id)->where('type', 'CMNT')->where('user_id', Auth::user()->id)->first();
    }
}
