<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Photo extends Model
{
    //

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getPhotoById($id)
    {
        return self::where('id', $id)->first();
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($photo) {
            //$file = File::get(storage_path() . '/app/' . $photo->link);
            File::delete(storage_path() . '/app/' . $photo->link);
        });
    }
}
