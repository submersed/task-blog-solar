<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($post) {
            $post->comment()->delete();
            $post->photo()->delete();
            $post->like()->delete();
        });
    }


    /*  public function getCreatedAtAttribute($value)
      {
          return Carbon::parse($value)->format('l jS \\of F Y h:i:s A');
      }*/

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function photo()
    {
        return $this->hasMany(Photo::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class)
            ->orderBy('created_at', 'desc')
            ->where('parent_id', null)
            ->with('user')
            ->with('like')
            ->with('post')
            ->with('children');
    }

    public function like()
    {
        return $this->hasMany(Like::class, 'target_id')->where('type', 'POST')->with('User');
    }


    public static function getPostById($post_id)
    {
        return self::where('id', $post_id)
            ->with('User')
            ->with('Photo')
            ->with('Like')
            ->with('Comment')
            ->first();
    }

    public static function getAllPost()
    {

        $ignors = Ignor::where('user_id', Auth::user()->id)->pluck('post_id')->all();
        $posts = self::whereNotIn('id', $ignors)
            ->orderby('created_at', 'desc')
            ->with('User')
            ->with('Photo')
            ->with('Like')
            ->with('Comment')
            ->get();


        return $posts;
    }
}
