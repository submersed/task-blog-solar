<?php

namespace App\Http\Controllers;

use App\Classes\Socket\Pusher;
use App\Ignor;
use App\Like;
use App\Photo;
use App\Post;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PostController extends Controller
{
    //
    public function createPost(Request $request)
    {
        $this->validate($request, [
            'content' => 'required|max:10000|min:10',

        ]);

        $post = new Post();
        $post->content = $request['content'];

            if ($post = ($request->user()->post()->save($post))) {

                /**
                 *    check if files exist in request
                 */

                if ($request->hasFile('file')) {
                    foreach ($request->file('file') as $file) {
                        $path = $file->store('public');
                        $photo = new Photo();
                        $photo->link = $path;
                        $photo->user_id = Auth::user()->id;
                        $post->photo()->save($photo);
                    }
                }

                $data = [
                    'topic_id' => 'post_crete',
                    'post_id' => $post->id,
                    'data' => json_encode(Post::getPostById($post->id))
                ];

                /**
                 *    Push  new post to dashboard
                 */

                Pusher::sendDataToServer($data);
            }
    }

    public function deletePost(Request $request)
    {

        $post = Post::getPostById($request['post_id']);
        if ($post->user_id == Auth::user()->id) {
            if ($post->delete()) {
                $data = [
                    'topic_id' => 'dashboard_update',
                    'data' => json_encode(Post::getAllPost())
                ];

                Pusher::sendDataToServer($data);
            }
        }
    }

    public function getAllPost()
    {
        return response()->json(Post::getAllPost());
    }

    public function doLike(Request $request)
    {

            $like = Like::getLike($request['post_id']);

            if (!$like) {
                $like = new Like();
                $like->user_id = Auth::user()->id;
                $like->type = 'POST';
                $like->target_id = $request['post_id'];
                $like->save();
            } else {
                $like->delete();
            }

            $data = [
                'topic_id' => 'post_update',
                'data' => json_encode(Post::getPostById($request['post_id']))
            ];

            Pusher::sendDataToServer($data);

    }

    public function doBlock(Request $request)
    {

            $ignor = new Ignor();
            $ignor->post_id = $request['post_id'];
            $ignor->user_id = Auth::user()->id;
            $ignor->save();

    }


    public function photoDelete(Request $request)
    {
            $photo = Photo::getPhotoById($request['id']);
            if ($photo && $photo->user_id == Auth::user()->id) {

                if ($photo->delete()) {
                    $data = [
                        'topic_id' => 'post_update',
                        'data' => json_encode(Post::getPostById($photo->post_id))
                    ];
                    Pusher::sendDataToServer($data);
                }
            }
    }


    public function postUpdate(Request $request)
    {
        $this->validate($request, [
            'content' => 'required|max:10000|min:10',
        ]);

        $post = Post::getPostById($request['id']);
        if ($post->user_id = Auth::user()->id) {
            $post->content = $request['content'];

            if ($post->update()) {

                /**
                 *    check if files exist in request
                 */

                if ($request->hasFile('file')) {
                    foreach ($request->file('file') as $file) {
                        $path = $file->store('public');
                        $photo = new Photo();
                        $photo->link = $path;
                        $photo->user_id = Auth::user()->id;
                        $post->photo()->save($photo);
                    }
                }

                $data = [
                    'topic_id' => 'post_update',
                    'post_id' => $post->id,
                    'data' => json_encode(Post::getPostById($request['id']))
                ];

                /**
                 *    Push  new post to dashboard
                 */

                Pusher::sendDataToServer($data);

            }

        }
    }
}
