<?php

namespace App\Http\Controllers;

use App\User;
use Guzzle\Http\Message\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function singIn(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:4'
        ]);

        if($result = Auth::attempt($request->only(['email', 'password']))){
                 return json_encode(Auth::user());
        }

    }

    public function singUp(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4',
            'name' => 'required|min:2'
        ]);

        $user = new User();
        $user->name = $request['name'];
        $user->password = bcrypt($request['password']);
        $user->email = $request['email'];

        if ($user->save()) {
            Auth::login($user);
            return json_encode(Auth::user());
        }

    }

    public function getUser(Request $request)
    {
        return  User::getUser($request);

    }
}
