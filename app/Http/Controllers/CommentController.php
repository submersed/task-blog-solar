<?php

namespace App\Http\Controllers;

use App\Classes\Socket\Pusher;
use App\Comment;
use App\Like;
use App\Post;
use Guzzle\Http\Message\Response;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{

    public function commentPost(Request $request)
    {

        $this->validate($request,[
            'comment'=>'required'
        ]);

        try {
            if ($post = Post::getPostById($request['post_id'])) {

                $user = Auth::user();
                $comment = new Comment();
                $comment->content = $request['comment'];
                $comment->user_id = $user->id;
                $comment->post_id = $post->id;

                if ($request->has('parent_id')) {
                    $comment->parent_id = $request['parent_id'];
                }

                if ($comment->save()) {

                    $data = [
                        'topic_id' => 'post_update',
                        'data' => json_encode(Post::getPostById($post->id))
                    ];

                    Pusher::sendDataToServer($data);
                }
                else{
                    return "err";
                }
            }
        } catch (QueryException $exception) {
            return $exception->getMessage();
        }
    }

    public function deleteComment(Request $request){

        $comment = Comment::getCommentById($request['comment_id']);

        if($comment->user_id== Auth::user()->id ||  $comment->post->user->id == Auth::user()->id ){
            if($comment->delete()){

                $data = [
                    'topic_id' => 'post_update',
                    'data' =>  json_encode(Post::getPostById($comment->post_id))
                ];

                Pusher::sendDataToServer($data);
            }
        }

    }

    public function doLike(Request $request)
    {

        try {
            $like = Like::getCommentLike($request['comment_id']);

            if (!$like) {
                $like = new Like();
                $like->user_id = Auth::user()->id;
                $like->type = 'CMNT';
                $like->target_id = $request['comment_id'];
                $like->save();
            } else {
                $like->delete();
            }

            $data = [
                'topic_id' => 'post_update',
                'data' => json_encode(Post::getPostById($request['post_id']))
            ];

            Pusher::sendDataToServer($data);
        }
        catch (QueryException $exception){
            return  response()->json(['sql_exc'=>$exception]);
        }

    }
}
