<?php

namespace App;

use Guzzle\Http\Message\Request;
use Illuminate\Database\QueryException;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public  function  post(){
       return $this->hasMany(Post::class);
    }

    public function  photo(){
        return $this->hasMany(Photo::class);
    }


    public function comment(){
        return $this->hasMany(Comment::class)->orderBy('created_at','desc');
    }

    public static function getUser($request){
        try {
            if (isset($request['id'])) {
                return self::where('id', $request['id'])->first();
            } else {
                return Auth::user();
            }
        }
        catch (QueryException $exception){
            return $exception->getMessage();
        }
    }
}
