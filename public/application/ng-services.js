/**
 * Created by sub on 23.01.2017.
 */
(function () {
    var service = angular.module('Services', []);

    service.service('ServiceAudio', [function () {
        var audio = {
            'message': new Audio('/audio/filling-your-inbox.mp3'),
            'like': new Audio('/audio/served.mp3')
        };

        return {
            all: function () {
                return audio;
            },
            message: function () {
                return audio.message;
            },
            like: function () {
                return audio.like;
            }
        }
    }]);

    service.service('ServiceUser', ['$http', function ($http) {
        var user = {};
        $http.get('user/get').then(
            function (data) {
                user = data.data;
                console.log(user);
            }
            , function (data) {

            });

        return {
            set:function (data) {
              user = data;
            },

            get:function () {

                return user;
            }
        }

    }])

})();