(function () {
    var login = angular.module('ModuleLogin', [])
        .controller('ModuleLoginController', ['$scope', function ($scope) {
            $scope.errors = {};
            $scope.$on('VALIDATE_RESULT', function (event, data) {
                $scope.errors = data;
            })
        }])
    /*
     .run(function ($window) {
     var connection = new $window.ab.connect(
     "ws://localhost:8080",
     function (session) {
     console.log('sub');
     session.subscribe('new_post', function (topic, data) {
     console.info('New data topic_id' + topic);
     console.log(data.data);

     });
     },
     function (code, rison, detail) {
     console.warn("Err:", code, rison, detail);
     },
     {
     'maxRetries': 10,
     'retryDelay': 4000,
     'skipSubprotocolCheck': true
     }
     )
     });
     */
    login.controller('SingUpController', ['$scope', '$http', '$location', 'ServiceUser',
        function ($scope, $http, $location, ServiceUser) {
            $scope.user = {};
            $scope.singUp = function () {
                $http.post('user/singup', $scope.user).then(function (data) {
                    if (typeof  data.data == "object") {
                        ServiceUser.set(data.data);
                        $location.url('/dashboard');
                    }
                    else {
                        $scope.$emit('VALIDATE_RESULT', {login: "wrong user data"});
                    }
                }, function (data) {
                    $scope.$emit('VALIDATE_RESULT', data.data);
                })
            }
        }]);

    login.controller('SingInController', ['$scope', '$http', '$location', 'ServiceUser', function ($scope, $http, $location, ServiceUser) {
        $scope.user = {};
        $scope.singIn = function () {
            console.info($scope.user);
            $http.post('user/singin', $scope.user).then(function (data) {
                    console.log(data);
                    if (typeof  data.data == "object") {
                        ServiceUser.set(data.data);
                        $location.url('/dashboard');
                    }
                    else if (data.data == "" || data.data == null) {
                        $scope.$emit('VALIDATE_RESULT', {login: "wrong user data"});
                    }

                }, function (data) {
                    console.log(data);
                    $scope.$emit('VALIDATE_RESULT', data.data);
                }
            )
        }
    }])
    ;
})
();