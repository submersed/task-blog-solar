(function () {
    var dashboard = angular.module('ModuleDashboard', []);


    dashboard.controller('DashboardController', ['$scope', '$rootScope', '$http', '$window', '$timeout', 'ServiceAudio', 'ServiceUser', function ($scope, $rootScope, $http, $window, $timeout, ServiceAudio, ServiceUser) {
        $scope.posts = [];
        $scope.errors = [];
        $scope.files = [];
        $scope.preview = [];

        $timeout(function () {
            $scope.user = ServiceUser.get();
        }, 1000);

        $scope.$on('POST_CREATE', function (event, data) {
            ServiceAudio.message().play();
            $scope.posts.push(data);
            $scope.posts.sort(function (a, b) {
                if (a.created_at > b.created_at) {
                    return -1;
                }
                if (a.created_at < b.created_at) {
                    return 1;
                }
                return 0;
            });

            $scope.post = [];
            $scope.files = [];
            $scope.preview = []
            ServiceAudio.message().play();
            $scope.$apply();
        });

        $scope.reverse = false;

        $scope.sortBy = function (propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        $scope.$on('POST_UPDATE', function (event, data) {
            ServiceAudio.message().play();

            for (var i = 0; i < $scope.posts.length; i++) {
                if ($scope.posts[i].id == data.id) {
                    $scope.posts[i] = data;
                    $scope.$apply();
                    return;
                }
            }

            $scope.$apply();
        });

        $scope.$on('DASHBOARD_UPDATE', function (event, data) {
            $scope.posts = data;
        });

        $http.get('post/all').then(function (data) {

                // console.log(data.data);
                $scope.posts = data.data;
            },
            function (data) {
                console.error(data);
            });

        $scope.createPreview = function (ele) {

            for (var i = 0; i < ele.files.length; i++) {
                $scope.files.push(ele.files[i]);
                $scope.preview.push(URL.createObjectURL(ele.files[i]));
            }
            $scope.$apply();
        };

        $scope.dropFileFromUpload = function (index) {
            $scope.files.splice(index, 1);
            $scope.preview.splice(index, 1);
        };

        $scope.sendPost = function () {
            var fd = new FormData();
            fd.append('content', $scope.post.content);

            for (var i = 0; i < $scope.files.length; i++) {
                fd.append('file[]', $scope.files[i]);
            }

            $http.post('post/create', fd,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }
            ).then(
                function () {
                    $scope.errors = [];
                },
                function (data) {
                    $scope.errors = data.data;
                }
            )
        };
    }]);

    dashboard.controller('PostCommentController', ['$scope', '$http', 'ServiceUser', function ($scope, $http, ServiceUser) {

        $scope.sub_comment = {};
        $scope.user = ServiceUser.get();

        $scope.commentComment = function (post_id, parent_id) {

            $scope.sub_comment.post_id = post_id;
            $scope.sub_comment.parent_id = parent_id;

            // console.log($scope.sub_comment);
            $http.post('comment/do', $scope.sub_comment).then(
                function (data) {
                },
                function (data) {
                    console.error(data);
                    $scope.errors = data.data;
                })

        };


        $scope.deleteComment = function (comment_id) {
            $http.delete('comment/delete', {
                    params: {
                        comment_id: comment_id
                    }
                }
            ).then(function () {

                },
                function (data) {
                    console.error(data);
                })
        };

        $scope.doCommentLike = function (post_id, comment_id) {
            $http.get('comment/like', {
                params: {
                    post_id: post_id,
                    comment_id: comment_id
                }
            }).then(function (data) {
                // console.log(data);

            }, function (data) {
                console.error(data);
            });
        };

        $scope.checkCommentLike = function (likes) {
            for (var i = 0; i < likes.length; i++) {
                if (likes[i].user.id == $scope.user.id) {
                    return true;
                }
            }
            return false;
        };

    }]);

    dashboard.controller('PostController', ['$scope', '$http', 'ServiceUser', '$rootScope', function ($scope, $http, ServiceUser, $rootScope) {
        $scope.comment = {};
        $scope.user = ServiceUser.get();
        $scope.postComment = function (post_id) {
            $scope.comment.post_id = post_id;
            $http.post('comment/do', $scope.comment).then(
                function (data) {

                },
                function (data) {
                    console.log(data);
                    $scope.errors = data.data;
                });
        };

        $scope.deletePost = function (post_id) {

            $http.delete('post/delete', {
                params: {
                    post_id: post_id
                }
            })
        };

        $scope.checkLike = function (likes) {
            for (var i = 0; i < likes.length; i++) {
                if (likes[i].user.id == $scope.user.id) {
                    return true;
                }
            }
            return false;
        };

        $scope.doLike = function (post_id) {

            $http.get('post/like', {
                params: {
                    post_id: post_id
                }
            }).then(function (data) {

            }, function (data) {
                console.error(data);
            });

        };

        $scope.doBlock = function (post_id) {
            $http.post('post/block', {post_id: post_id}).then(function () {
                for (var i = 0; i < $scope.posts.length; i++) {
                    if (post_id == $scope.posts[i].id) {
                        $scope.posts.splice($scope.posts.indexOf($scope.posts[i]), 1);
                        return;
                    }
                }
            }, function (data) {
                console.error(data);
            })
        };

        $scope.editPost = function (_post) {

            $rootScope.$emit('POST_EDIT', _post);
            $('#post-edit').modal('show');
        };

    }]);


    dashboard.controller('PostEditController', ['$scope', '$http', '$rootScope','ServiceUser','$timeout', function ($scope, $http, $rootScope,ServiceUser,$timeout) {

        $scope.files = [];
        $scope.preview = [];


        $timeout(function () {
            $scope.user = ServiceUser.get();
        }, 1000);

        $rootScope.$on('POST_EDIT', function (event, data) {
            $scope.targetPost = data;
        });

        $scope.$on('POST_UPDATE', function (event, data) {
            console.log(data);
            console.log($scope.user);
            if($scope.user.id == data.user.id) {
                $scope.targetPost = data;
                $scope.$apply();
            }
        });

        $scope.removePhoto = function (id) {

            $http.delete('post/photo/delete', {
                params: {
                    id: id
                }
            }).then(function () {

            }, function (data) {
                console.error(data);
            })
        };

        $scope.postSave = function () {
            console.log($scope.targetPost.content);

            var fd = new FormData();
            fd.append('content', $scope.targetPost.content);
            fd.append('id', $scope.targetPost.id);

            for (var i = 0; i < $scope.files.length; i++) {
                fd.append('file[]', $scope.files[i]);
            }
            ;

            $http.post('post/update', fd,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }
            ).then(function (data) {
                    console.log(data)
                    $scope.preview = [];
                    $scope.files = [];
                },
                function (data) {
                    console.error(data);
                })
        };

        $scope.createPreview = function (ele) {
            for (var i = 0; i < ele.files.length; i++) {
                $scope.files.push(ele.files[i]);
                $scope.preview.push(URL.createObjectURL(ele.files[i]));
                console.log($scope.files);
            }
            $scope.$apply();
        }
    }]);
})();
