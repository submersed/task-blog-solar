/**
 * Created by sub on 24.01.2017.
 */
(function () {
    var d = angular.module('Directives', [])
    d.directive('autoresize', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    var element = typeof event === 'object' ? event.target : document.getElementById(event);
                    var scrollHeight = element.scrollHeight;
                    if (scrollHeight > 60) {
                        element.style.height = scrollHeight + "px";
                    }
                });


            }
        }
    });
    d.directive('dirComment', function () {
        return {
            scope: {
                comment: '=',

            },
            templateUrl: "template/dir_comment",
            controller:'PostCommentController'
        }
    })
})();