(function () {
    'use strict'
    angular.module('ApplicationBlog',
        [
            'ngRoute',
            'ModuleLogin',
            'ModuleDashboard',
            'Services',
            'Directives',
            'xeditable'
        ]
        )
        .run(function(editableOptions) {
            editableOptions.theme = 'default';
        })
        .config(function ($routeProvider, $locationProvider) {
            $routeProvider
                .when('/', {
                    redirectTo: '/dashboard',
                })
                .when('/dashboard', {
                    templateUrl: 'template/dashboard',
                    controller: 'DashboardController',
                })
                .when('/login', {
                    templateUrl: 'template/login',

                })
                .when('/logout', {
                    redirectTo: '/logout',

                })
                .otherwise({templateUrl: 'template/404'});
            $locationProvider.html5Mode(true);


        })

        .controller('ApplicationBlogController', ['$scope', '$window', 'ServiceUser', function ($scope, $window, ServiceUser) {


            new $window.ab.connect(
                "ws://" + $window.location.hostname + ":8080",
                function (session) {
                    session.subscribe('post_crete', function (topic, data) {
                        console.log(JSON.parse(data.data));
                        $scope.$broadcast('POST_CREATE', JSON.parse(data.data));
                    });
                    session.subscribe('post_update', function (topic, data) {
                        console.log(JSON.parse(data.data));
                        $scope.$broadcast('POST_UPDATE', JSON.parse(data.data));
                    });
                    session.subscribe('dashboard_update',function (topic, data) {
                        console.log(JSON.parse(data.data));
                        $scope.$broadcast('DASHBOARD_UPDATE',JSON.parse(data.data))
                    })
                },
                function (code, rison, detail) {
                    console.warn("Err:", code, rison, detail);
                },
                {
                    'maxRetries': 100,
                    'retryDelay': 4000,
                    'skipSubprotocolCheck': true
                }
            )

        }]);
})();