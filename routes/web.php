<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('master');
})->middleware('auth');


Route::get('login', function () {
    return view('master');
});

Route::get('logout', function () {
    Auth::logout();
    return redirect('/');
})->middleware('auth');

Route::get('info', function () {
    return Auth::user()->name;
});

Route::get('/{all}', function () {
    return view('master');
})->middleware('auth');

/**
 *   User's actions
 **/

Route::post('user/singup', ['uses' => 'UserController@singUp']);
Route::post('user/singin', ['uses' => 'UserController@singIn']);
Route::get('user/get',['uses'=>'UserController@getUser']);

/**
 * Post's actions
 *
 */
Route::post('post/create', ['uses' => 'PostController@createPost']);
Route::get('post/all', ['uses' => 'PostController@getAllPost']);
Route::delete('post/delete',['uses'=>'PostController@deletePost']);
Route::get('post/like',['uses'=>'PostController@doLike']);
Route::post('post/block',['uses'=>'PostController@doBlock']);
Route::delete('post/photo/delete',['uses'=>'PostController@photoDelete']);
Route::post('post/update',['uses'=>'PostController@postUpdate']);

/**
 *  Comment's actions
 * */
Route::post('comment/do',['uses'=>'CommentController@commentPost']);
Route::delete('comment/delete',['uses'=>'CommentController@deleteComment']);
Route::get('comment/like',['uses'=>'CommentController@doLike']);


/**
 *    Angular's templates
 */

Route::get('template/{template}', function ($template) {
    try {
        return File::get(resource_path() . '/views/templates/' . $template . ".html");
    } catch (FileNotFoundException $exception) {
        return File::get(resource_path() . '/views/templates/404.html');
    }
});

/**
 *    Get files route's
 */

Route::get('file/{storage}/{image}', function ($storage, $image) {

    $path = storage_path() . '/app/' . $storage . '/' . $image;
    try{
        $file = File::get($path);
        return response($file, 200)->header('Content-Type', 'text/plain');
    }
    catch (FileNotFoundException $exception){
        return response($exception->getMessage());
    }

});


